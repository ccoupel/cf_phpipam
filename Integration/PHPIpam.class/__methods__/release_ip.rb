#
# Description: <Method description here>
#
  
  @debug = true
$evm.log('info', "##################################################################################################")
$evm.log(:info, "Starting PHP IPAM Integration")
$evm.log('info', "##################################################################################################")
require 'net/http'
require 'json'
#vlan = "VM Network VLAN 10"
#****
#****

def getURI(action)
  url = $evm.object['url'] + "/api/" + $evm.object['context'] + "/#{action}/"
  URI(url)
end

def getHTTP(action)
  uri=getURI(action)
  Net::HTTP.new(uri.host, uri.port)
end

def htmlPOST(action,query=nil, token=nil)
  uri=getURI(action)
  token=getToken if token.nil?
  header = {'Content-Type': 'text/json', "token": token}
  request = Net::HTTP::Post.new(uri.request_uri, header)
  request.basic_auth $evm.object['user'], $evm.object.decrypt('password')
  request.body = query.to_param unless query.nil?
  response = getHTTP(action).request(request)
  JSON.parse(response.body) rescue nil
end

def htmlGET(action)
  uri=getURI(action)
  header = {'Content-Type': 'text/json', "token": getToken}
  request = Net::HTTP::Get.new(uri.request_uri, header)

  response = getHTTP(action).request(request)
  JSON.parse(response.body) rescue nil
end

def htmlDELETE(action)
  uri=getURI(action)
  header = {'Content-Type': 'text/json', "token": getToken}
  request = Net::HTTP::Delete.new(uri.request_uri, header)

  response = getHTTP(action).request(request)
  JSON.parse(response.body) rescue nil
end

def getToken
  #url="http://10.217.3.99:801/api/cfme/user/"; uri=URI(url);http = Net::HTTP.new(uri.host, uri.port);header = {'Content-Type': 'text/json'};request = Net::HTTP::Post.new(uri.request_uri, header);request.basic_auth "admin","ErtyDfgh";response = http.request(request);puts response.body
  response=htmlPOST("user",{}, "")
  response["data"]["token"] rescue nil
  
end

def getIPid(ip)
  $evm.log('info', "##################################################################################################")
  $evm.log(:info, "IPAM Entering getIPid for #{ip}")
  $evm.log('info', "##################################################################################################")

  response=htmlGET("/addresses/search/#{ip}")
  unless response["data"].nil?
    return response["data"].first["id"]
  end
end

def deleteAddress(id)
  $evm.log('info', "##################################################################################################")
  $evm.log(:info, "IPAM Entering deleteAddress for id #{id}")
  $evm.log('info', "##################################################################################################")
  htmlDELETE("/addresses/#{id}")
end


begin
    prov = $evm.root['miq_provision'] || $evm.root['vm'].miq_provision
  ipaddr = prov.get_option(:ip_addr)
  $evm.log(:info, "Starting method to delete IP #{ipaddr}")
  ip_id=getIPid(ipaddr)
  response=deleteAddress(ip_id)
    unless response["data"].nil?
       $evm.log(:info," IP #{ip} removed successfully")
    end
  
    $evm.root['ae_result']         = "ok"
    $evm.root['ae_retry_interval'] = 20.seconds
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "error"
    $evm.root['ae_retry_interval'] = 20.seconds
    exit MIQ_OK
end
