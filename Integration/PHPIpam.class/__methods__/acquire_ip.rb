#
# Description: get IP from PHP IPAM
#
@debug = true
$evm.log('info', "##################################################################################################")
$evm.log(:info, "Starting PHP IPAM Integration")
$evm.log('info', "##################################################################################################")
require 'net/http'
require 'json'
#vlan = "VM Network VLAN 10"
#****
#****

def getURI(action)
  url = $evm.object['url'] + "/api/" + $evm.object['context'] + "/#{action}/"
  URI(url)
end

def getHTTP(action)
  uri=getURI(action)
  Net::HTTP.new(uri.host, uri.port)
end

def htmlPOST(action,query=nil, token=nil)
  uri=getURI(action)
  token=getToken if token.nil?
  header = {'Content-Type': 'text/json', "token": token}
  request = Net::HTTP::Post.new(uri.request_uri, header)
  request.basic_auth $evm.object['user'], $evm.object.decrypt('password')
  request.body = query.to_param unless query.nil?
  response = getHTTP(action).request(request)
  JSON.parse(response.body) rescue nil
end

def htmlGET(action)
  uri=getURI(action)
  header = {'Content-Type': 'text/json', "token": getToken}
  request = Net::HTTP::Get.new(uri.request_uri, header)

  response = getHTTP(action).request(request)
  JSON.parse(response.body) rescue nil
end

def getToken
  #url="http://10.217.3.99:801/api/cfme/user/"; uri=URI(url);http = Net::HTTP.new(uri.host, uri.port);header = {'Content-Type': 'text/json'};request = Net::HTTP::Post.new(uri.request_uri, header);request.basic_auth "admin","ErtyDfgh";response = http.request(request);puts response.body
  response=htmlPOST("user",{}, "")
  response["data"]["token"] rescue nil
  
end

def getSections()
  $evm.log('info', "##################################################################################################")
  $evm.log(:info, "IPAM Entering getSections")
$evm.log('info', "##################################################################################################")

  sections = Array.new

  htmlGET("sections")["data"].each do |a|
    sections.push(a["id"])
  end
  sections
end

def getSubnets(sections)
  $evm.log('info', "##################################################################################################")
  $evm.log(:info, "IPAM Entering getSubnets")
  $evm.log('info', "##################################################################################################")
  subnets = Array.new

  sections.each do |sectionID|
    response=htmlGET("sections/#{sectionID}/subnets/")
    unless response.nil?
      response["data"].each do |a|
         subnets.push(a["id"])
       end
    end
  end
  subnets
end

def getValues(vlan,subnets)
  $evm.log('info', "##################################################################################################")
  $evm.log(:info, "IPAM Entering findSubnetMatch")
  $evm.log('info', "##################################################################################################")
  #prov = $evm.root['miq_provision']  

  subnets.each do |subnet|
    value=nil
    response = htmlGET("subnets/#{subnet}/")
    unless response["data"].nil?
      if response["data"]["description"] == vlan
        value = response
        return value
      end
    end
  end
  return value
end

def reserveFirstAddress(subnet,fqdn)
  $evm.log('info', "##################################################################################################")
  $evm.log(:info, "IPAM Entering reserveFirstAddress")
  $evm.log('info', "##################################################################################################")
  #prov = $evm.root['miq_provision']  
  ip = String.new

  response = htmlPOST("addresses/first_free/#{subnet}",{"description"=>"CFME","hostname"=>fqdn})
  $evm.log(:info, "reserveFirstAddress #{subnet} => #{response}")
  unless response["data"].nil?
    ip=response["data"]
    $evm.log(:info, "reserveFirstAddress #{ip} => #{response}")
  end
  ip
end

################################################## Main section ################################################
#****

begin

if $evm.root["vm"].nil?
  prov = $evm.root['miq_provision']
  vmtargetname = prov.get_option(:vm_target_name)
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "IPAM variables 1 : Prov=#{prov}, vmtargetname=#{vmtargetname}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
else
  prov = $evm.root["vm"]
  vmtargetname = $evm.root["name"]
  
  $evm.log('info', "##################################################################################################") if @debug
  $evm.log(:info, "IPAM variables 2 : Prov=#{prov},  vmtargetname=#{vmtargetname}") if @debug
  $evm.log('info', "##################################################################################################") if @debug
end
subnet_name = prov.get_option(:vlan)
#subnet_name = "dev86"
$evm.log('info', "##################################################################################################") if @debug
$evm.log(:info, "IPAM variables : Prov=#{prov}, subnet_name=#{subnet_name}, vmtargetname=#{vmtargetname}") if @debug
$evm.log('info', "##################################################################################################") if @debug
#****

#Get a list of all sections. Returns an array of all sections
sections = getSections()

#Get array of all subnets from all sections
subnets = getSubnets(sections)

# Get the values from PHPIpam like Subnet id,Mask, Gateway, ...
value = getValues(subnet_name,subnets)

$evm.log('info', "##################################################################################################")
$evm.log(:info, "Got following from IPAM: value=#{value}")
$evm.log('info', "##################################################################################################")

#Set the provisioning template
#Get the subnetID of the subnet that matches the subnet_name from provisioning request (Description of subnet in phpipam must match the VM Network tag in Vmware)
foundSubnet = value['data']['id']
subnet_addr = value['data']['subnet']
subnet_mask = value['data']['calculation']['Subnet netmask']
gateway = value['data']['gateway']['ip_addr'] rescue nil
dns_domain=value['data']["nameservers"]["name"] rescue nil
nameservers=value['data']["nameservers"]["namesrv1"] rescue nil
  fqdn = "#{vmtargetname}.#{dns_domain}"
  
#Reserve the IP address in phpipam
ip_addr = reserveFirstAddress(foundSubnet,fqdn)

$evm.log('info', "##################################################################################################") if @debug
$evm.log(:info, "Got following from IPAM for #{fqdn}: subnet=#{foundSubnet}, ip_addr=#{ip_addr}, subnet_addr=#{subnet_addr}/#{subnet_mask}, gateway=#{gateway},dns_domain=#{dns_domain} dns_servers=#{nameservers}") if @debug
$evm.log('info', "##################################################################################################") if @debug

# Set values to VM

prov.set_option(:addr_mode, ["static", "static"])
prov.set_option(:ip_addr, "#{ip_addr}")
prov.set_option(:subnet_addr,  "#{subnet_addr}")
prov.set_option(:subnet_mask,  "#{subnet_mask}")
prov.set_option(:gateway,  "#{gateway}")

prov.set_option(:dns_domain,  "#{dns_domain}")
prov.set_option(:dns_servers,  "#{nameservers.tr(";",",")}")
#prov.set_option(:dns_servers,  "8.8.4.4,4.4.4.4")
prov.set_option(:dns_suffixes, "#{dns_domain}")
prov.set_option(:vm_targetname, "#{fqdn}")

=begin
prov.set_network_address_mode('static')

prov.set_nic_settings(0, {
  :ip_addr		=> ip_addr,
  :subnet_addr  => subnet_addr,
  :subnet_mask	=> subnet_mask,
  :gateway		=> gateway.to_s,
  :dns_domain	=> dns_domain,
  })
=end
  
$evm.log('info', "##################################################################################################") if @debug
$evm.log('info', "IPAM => SET IP: #{prov.inspect}") if @debug
$evm.log('info', "##################################################################################################") if @debug

      $evm.root['ae_result']         = "ok"
    $evm.root['ae_retry_interval'] = 20.seconds
  
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "error"
    $evm.root['ae_retry_interval'] = 20.seconds
    exit MIQ_OK
end
